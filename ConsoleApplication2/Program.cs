﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication1
{
    public delegate void Nab(object ob);
    class Fakultet
    {
        string name, telefone, fam;
        int premija;
        Nab Nab_li_prm, Nab_li_name;
        public Fakultet(string nm, string f, string tlfn)
        {
            name = nm;
            fam = f;
            telefone = tlfn;
        }
        public void Registracija_prm(Nab N)
        {
            Nab_li_prm += N;
        }
        public void Registracija_name(Nab N)
        {
            Nab_li_name += N;
        }
        public void Prem(int procent)
        {
            premija = procent;
            if (Nab_li_prm != null)
                Nab_li_prm(this);
        }
        public void Nazv(string n)
        {
            name = n;
            if (Nab_li_name != null)
                Nab_li_name(this);
        }
        public string Name
        {
            get { return name; }
        }
        public int Premija
        {
            get { return premija; }
        }
        public string Telefone
        {
            get { return telefone; }
        }
        public string Fam
        {
            get { return fam; }
        }
        public void vivod()
        {
            Console.WriteLine("|{0,-10} | {1,-13} |{2,-10}|", name, fam, telefone);
        }
    }
    class Student : IComparable
    {
        string imya, naz_fakl;
        int step;
        int[] ocenki;
        public Student(string fm, string n, int[] c, int st)
        {
            imya = fm;
            naz_fakl = n;
            ocenki = new int[4];
            ocenki = c;
            step = st;
        }
        public double sr_b
        {
            get
            {
                double s = 0;
                foreach (int x in ocenki) s = s + x;
                return s / ocenki.Length;
            }
        }
        public void vivod()
        {
            Console.WriteLine("|{0,-10} | {1,-12} |{2,-10}|", imya, sr_b, step);
        }
        public void PR(int x)
        {
            step = x + step;
        }
        public void R_N(object ob)
        {
            naz_fakl = ((Fakultet)ob).Name;
        }
        public string Naz_fkl
        {
            get { return naz_fakl; }
        }
        public int Step
        {
            get { return step; }
        }
        public int CompareTo(object obj)
        {
            Student std = obj as Student;
            return naz_fakl.CompareTo(std.naz_fakl);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader f1 = new StreamReader("baza2.txt");
            string s = f1.ReadLine(); int j = 0;
            while (s != null)
            {
                s = f1.ReadLine();
                j++;
            }
            f1.Close();
            Fakultet[] fk = new Fakultet[j];
            f1 = new StreamReader("baza2.txt");
            s = f1.ReadLine();
            j = 0;
            while (s != null)
            {
                string[] ss = s.Split(';');
                fk[j] = new Fakultet(ss[0], ss[1], ss[2]);
                s = f1.ReadLine();
                j++;
            }
            f1.Close();
            Console.WriteLine("|  Название |   Фамилия     | Телефон  |");
            Console.WriteLine("|-----------|---------------|----------|");
            foreach (Fakultet otd in fk) otd.vivod();
            Console.WriteLine("|--------------------------------------|");
            StreamReader f2 = new StreamReader("baza1.txt");
            s = f2.ReadLine(); j = 0;
            while (s != null)
            {
                s = f2.ReadLine();
                j++;
            }
            f2.Close();
            Student[] st = new Student[j];
            f2 = new StreamReader("baza1.txt");
            s = f2.ReadLine(); j = 0;
            while (s != null)
            {
                string[] ss = s.Split(';');
                int[] c = new int[4];
                for (int ii = 0; ii < 4; ii++)
                {
                    c[ii] = Convert.ToInt32(ss[ii + 2]);
                }
                st[j] = new Student(ss[0], ss[1], c, Convert.ToInt32(ss[6]));
                int i = 0;
                while (i < fk.Length)
                {
                    if (st[j].Naz_fkl == fk[i].Name)
                    {
                        fk[i].Registracija_name(st[j].R_N);///////
                        i = fk.Length;
                    }
                    i++;
                }
                s = f2.ReadLine();
                j++;
            }
            f2.Close();
            Array.Sort(st);
            Console.WriteLine(st[0].Naz_fkl);
            Console.WriteLine("|  Фамилия  |  Средний бал | Степендия|");
            Console.WriteLine("|-----------|--------------|----------|");
            st[0].vivod();
            for (int i = 1; i < st.Length; i++)
            {
                if (st[i].Naz_fkl != st[i - 1].Naz_fkl)
                    Console.WriteLine(st[i].Naz_fkl);
                st[i].vivod();
            }
            Console.WriteLine("|-----------|--------------|----------|");
            Console.WriteLine(" Название факультета?");
            string s_o = Console.ReadLine();
            Console.WriteLine(" Новое название?");
            string t_o = Console.ReadLine();
            foreach (Fakultet otd in fk)
            {
                if (otd.Name == s_o) otd.Nazv(t_o);
            }
            Console.Clear();
            Console.WriteLine(st[0].Naz_fkl);
            st[0].vivod();
            for (int i = 1; i < st.Length; i++)
            {
                if (st[i].Naz_fkl != st[i - 1].Naz_fkl)
                    Console.WriteLine(st[i].Naz_fkl);
                st[i].vivod();
            }
            Console.WriteLine("На сколько увеличиваем стипендию?");
            int stip = Convert.ToInt32(Console.ReadLine());
            foreach (Student otd in st)
            {
                if (otd.sr_b > 7)
                    otd.PR(stip);
            }
            //foreach (Student std in st)
                //std.vivod();
            for (int i = 1; i < st.Length; i++)
            {
                if (st[i].Naz_fkl != st[i - 1].Naz_fkl)
                    Console.WriteLine(st[i].Naz_fkl);
                st[i].vivod();
            }
            Console.ReadKey();
        }
    }
}
