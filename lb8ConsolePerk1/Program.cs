﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace lb8ConsolePerk1
{
    class Program
    {

        struct SPORT : IComparable
        {
            public string fam;
            public string born;
            public KindOfSport KoS;
            public GradeOfSport GoS;
            public int vozrast
            {
                get
                {
                    return (DateTime.Now.Year - Convert.ToDateTime(born).Year);
                }
            }
            public int CompareTo(Object obj)
            { SPORT st = (SPORT)obj; return 0; }
        }
        public enum KindOfSport
        {
            Легкая_атлетика, Бокс, Плавание, Гимнастика, Штанга
        };
        public enum GradeOfSport
        {
            Iразряд, IIразряд, IIIразряд, КМС, МС, МСМК
        };
        static void Main(string[] args)
        {
            Console.WriteLine("Введите имя файла для ввода информации");
            string name = Console.ReadLine();
            try
            {
                StreamReader File = new StreamReader(name);
                string f = File.ReadLine();
                int j = 0;
                while (f != null)
                {
                    f = File.ReadLine();
                    j++;
                }
                File.Close();
                SPORT[] sprt = new SPORT[j];
                string[] makingstruct = new string[4];
                File = new StreamReader(name);
                f = File.ReadLine(); j = 0;
                while (f != null)
                {

                    makingstruct = f.Split(';');
                    sprt[j].fam = makingstruct[0];
                    sprt[j].born = makingstruct[1];
                    sprt[j].GoS = (GradeOfSport)Enum.Parse(typeof(GradeOfSport), makingstruct[2]);
                    sprt[j].KoS = (KindOfSport)Enum.Parse(typeof(KindOfSport), makingstruct[3]);
                    f = File.ReadLine();
                    j++;
                }
                File.Close();
                for (int i = 0; i < j; i++)
                {
                    Console.WriteLine(sprt[i].fam + "." + sprt[i].born + "." + sprt[i].GoS + "." + sprt[i].KoS);
                }
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("Выберите вид спорта,о спортсменах которого вы хотите получить инфу");
                Console.ReadKey();

                ConsoleKeyInfo input; int k = 0;
                do
                {
                    Console.Clear();

                    for (int i = 0; i < 5; i++)
                        if (i == k)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                            string s = Enum.GetName(typeof(KindOfSport), i);
                            Console.WriteLine(s);
                            Console.ResetColor();
                        }
                        else
                        {
                            string s = Enum.GetName(typeof(KindOfSport), i);
                            Console.WriteLine(s);
                        }


                    Console.WriteLine("Press Enter to choose.");
                    Console.ResetColor();
                    input = Console.ReadKey(true);
                    if (input.Key == ConsoleKey.DownArrow) { if (k < 4)k++; };
                    if (input.Key == ConsoleKey.UpArrow) { if (k > 0)k--; };
                    Console.WriteLine();
                } while (input.Key != ConsoleKey.Enter);
                for (int i = 0; i < j; i++)
                {
                    if (Convert.ToString(sprt[i].KoS) == Enum.GetName(typeof(KindOfSport), k))
                        Console.WriteLine(sprt[i].fam + "." + sprt[i].born + "." + sprt[i].GoS + "." + sprt[i].KoS);
                }
                Console.ReadKey();
                Console.WriteLine("для продолжения нажмите на любую клавишу");
                for (int MK = 0; MK < 5; MK++)
                {
                    string namefile = Enum.GetName(typeof(GradeOfSport), MK);
                    StreamWriter f1 = new StreamWriter(namefile + ".txt");
                    f1.WriteLine("Iразряд");
                    f1.WriteLine("легкая атлетика        ");
                    f1.WriteLine("_______________________");
                    f1.WriteLine("|№|Фамилия     |Возраст");
                    f1.WriteLine("_______________________");
                    k = 0;
                    int flag = 0;
                    for (int i = 0; i < sprt.Length; i++)
                    {

                        if (Convert.ToString(sprt[i].KoS) == Enum.GetName(typeof(KindOfSport), 0) && Convert.ToString(sprt[i].GoS) == Enum.GetName(typeof(GradeOfSport), MK))
                        {
                            k++;
                            f1.WriteLine("|{0,1}|{1,12}|{2,7}|", k, sprt[i].fam, sprt[i].vozrast);
                            flag = 1;
                        }

                    }
                    f1.WriteLine("Бокс        ");
                    f1.WriteLine("_______________________");
                    f1.WriteLine("|№|Фамилия     |Возраст");
                    f1.WriteLine("_______________________");
                    k = 0;
                    for (int i = 0; i < sprt.Length; i++)
                    {

                        if (Convert.ToString(sprt[i].KoS) == Enum.GetName(typeof(KindOfSport), 1) && Convert.ToString(sprt[i].GoS) == Enum.GetName(typeof(GradeOfSport), MK))
                        {
                            k++;
                            f1.WriteLine("|{0,1}|{1,12}|{2,7}|", k, sprt[i].fam, sprt[i].vozrast);
                            flag = 1;
                        }

                    }
                    f1.WriteLine("Плавание       ");
                    f1.WriteLine("_______________________");
                    f1.WriteLine("|№|Фамилия     |Возраст");
                    f1.WriteLine("_______________________");
                    k = 0;
                    for (int i = 0; i < sprt.Length; i++)
                    {

                        if (Convert.ToString(sprt[i].KoS) == Enum.GetName(typeof(KindOfSport), 2) && Convert.ToString(sprt[i].GoS) == Enum.GetName(typeof(GradeOfSport), MK))
                        {
                            k++;
                            f1.WriteLine("|{0,1}|{1,12}|{2,7}|", k, sprt[i].fam, sprt[i].vozrast);
                            flag = 1;
                        }

                    }
                    f1.WriteLine("Гимнастика        ");
                    f1.WriteLine("_______________________");
                    f1.WriteLine("|№|Фамилия     |Возраст");
                    f1.WriteLine("_______________________");
                    k = 0;
                    for (int i = 0; i < sprt.Length; i++)
                    {

                        if (Convert.ToString(sprt[i].KoS) == Enum.GetName(typeof(KindOfSport), 3) && Convert.ToString(sprt[i].GoS) == Enum.GetName(typeof(GradeOfSport), MK))
                        {
                            k++;
                            f1.WriteLine("|{0,1}|{1,12}|{2,7}|", k, sprt[i].fam, sprt[i].vozrast);
                            flag = 1;
                        }

                    }
                    f1.WriteLine("штанга        ");
                    f1.WriteLine("_______________________");
                    f1.WriteLine("|№|Фамилия     |Возраст");
                    f1.WriteLine("_______________________");
                    k = 0;
                    for (int i = 0; i < sprt.Length; i++)
                    {

                        if (Convert.ToString(sprt[i].KoS) == Enum.GetName(typeof(KindOfSport), 4) && Convert.ToString(sprt[i].GoS) == Enum.GetName(typeof(GradeOfSport), MK))
                        {
                            k++;
                            f1.WriteLine("|{0,1}|{1,12}|{2,7}|", k, sprt[i].fam, sprt[i].vozrast);
                            flag = 1;
                        }

                    }
                    if (flag == 0)
                        f1.WriteLine("Нет данных по данному разряду");
                    f1.Close();
                }
            }
            catch (FileNotFoundException) { Console.WriteLine("File not found"); }


        }
    }

}